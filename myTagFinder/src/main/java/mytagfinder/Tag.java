package mytagfinder;

import java.util.ArrayList;

public class Tag {
    //un tag è una lista di oggetti (KeyWords)
    ArrayList<String> list = new ArrayList<String>();
    String name;
    Boolean found = false;

    public Tag(String _name){
        name=_name;
    }

    public Tag(String _name, String _text){
        name=_name;
        extractTags(_text);
    }

    public void addKeyWord(String keyWord) {
        list.add(keyWord);
    }

    public void extractTags(String text) {
        for (String retval: text.split("[,]")) {
            String after = retval.trim().replaceAll(" +", "");
            addKeyWord(after);
        }
    }

    public void check(String word){
        //controlliamo se una parola si trova all'interno delle key words per un tag
        for (int i=0; i<list.size(); i++){
            if((found==false) && (word.contains(list.get(i)))){
                /*
                System.out.print(word);
                System.out.print(":");
                System.out.println(name);
                 */
                found = true;
                break;
            }
        }
    }

    public void printKeys(){
        //controlliamo se una parola si trova all'interno delle key words per un tag
        System.out.println(" ");
        System.out.print(name + ": ");
        for (int i=0; i<list.size(); i++){
            System.out.print(list.get(i) + ",");
        }
    }
}
