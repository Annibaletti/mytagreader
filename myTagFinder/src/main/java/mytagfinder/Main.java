package mytagfinder;
    /*
    https://github.com/tesseract-ocr/tessdoc
    */

import java.io.Console;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static void main(String [] args) throws Exception{

        Image image = new Image("/Users/francescaannibaletti/Desktop/alltirocinio/menutest/1.png");
        image.lookForTag();
        image.printTags();
    }
}
