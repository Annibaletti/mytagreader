package mytagfinder;

import java.util.List;
import java.util.ArrayList;

//classe che definisce tutti i tag e le keywords contenute
public class TagDefiner {

    List<Tag> list = new ArrayList<Tag>();

    public void addTag(Tag tag) {
        list.add(tag);
    }

    public TagDefiner(){
        Tag pizza = new Tag("pizza", "pizz, margherita, capricciosa");
        Tag vegetariano = new Tag("vegetariano", "vegetarian, veggie, vegan");
        Tag formaggi = new Tag("formaggi", "asiago, brie, burrata, caciocavallo, caciotta, caprino, cheddar, adamer, emmenthal," +
                "fontina, formaggella, formaggino, formaggio, gorgonzola, grana, groviera, montasio, mozzarella, " +
                "parmigiano, pecorino, provola, provolino, provolone, quartirolo, ricotta, robiola, roquefort, scamorza, " +
                "sottilette, stracchino, stracciatella, burrata, taleggio, tomino, certosino, crescenza, feta, philadelphia, " +
                "mascarpone, tosella");
        Tag verdura = new Tag("verdura", "aglio, agretti, asparagi, barbabietola, bieta, bietole, borragine, broccoletti, broccoli, " +
                "broccol, bruscandoli, carciofi, carciofini, carciofo, cardi, cardo, cardetti, carot, castradure, catalogna, cavolfiore, cavolini, " +
                "cavolo, cetrioli, cetriolini, cetriolo, cicoria, cicorino, cime, cipolla, cipolle, cipollina, cipolline, cipollotti, cipollotto, " +
                "crauti, cren, daikon, fagiolini, finocchi, finocchio, friarelli, friggitelli, halapegno, invidia, insalata, insalatina, lattuga, lattughino, " +
                "manioca, melanzana, melanzane, misticanza, pannocchiette, patata, patate, peperone, peperoni, pomodori, pomodorini, pomodoro, " +
                "porri, porro, prezzemolo, puntarelle, rabarbaro, radicchio, rafano, rapa, rape, ravanelli, ravanello, rucola, salicornia, scalogno, " +
                "scarola, scorzonera, sedano, songino, spinaci, spinacino, taccole, topinambur, tapioca, tegoline, valeriana, verdure, verza, zucca, zucchine, " +
                "zucchina, sottaceti");
        Tag carne = new Tag("carne", "agnello, pollo, anatra, bovino, bue, cappone, capretto, carne, cavallo, maiale, " +
                "manzo, struzzo, vitello, vitellone, coniglio, coratella, prosciutto, coscia, manzo, costata, costine, cotenna, costolette, " +
                "gallo, guancia, lingua, lonza, ossobuco, petto, anatra, oca, porchetta, quaglie, rana, scamone, capriolo, cervo," +
                "cinghiale, daino, fagiano, faraona, lepre");
        Tag selvaggina = new Tag("selvaggina", "capriolo, cervo, cinghiale, daino, fagiano, faraona, lepre");
        Tag salumi = new Tag("salumi", "bacon, bresaola, capicollo, carne, coppa, cotechino, guanciale, lardo, lucanica, mortadella, nduja, " +
                "pancetta, pitina, prosciutto, salame, salamella, salamino, salsicce, salsiccette, salsiccia, soppressa, speck, wrustel");
        Tag pesce = new Tag("pesce", "acciuga, acciughe, acquadelle, alalunga, alici, anguilla, aringa, baccal, bianchetti, " +
                "bottarga, branzino, carpa, cavedano, caviale, cernia, dentice, gallinella, luccio, lucerna, merluzzo, nasello, ombrina, orata, " +
                "pagello, palombo, passerino, pesce, persico, prete, spada, spatola, platessa, pescatrice, rana, ricciola, rombo, salmerino, salmone, " +
                "sarda, sarde, sardina, scorfano, sgombro, soaso, sogliola, spigola, stoccafisso, surimi, tinca, tonno, triglia, trota, volpina, zanchetta, " +
                "alici");
        Tag spezie = new Tag("spezie", "aglio, origano, basilico, rosmarino, vaniglia, salvia, ginepro, alloro, curry, " +
                "cumino");
        Tag piccante = new Tag("piccante", "piccante, peperoncin, diavola");
        Tag dolci = new Tag("dolci", "dessert, dolc, cannol, pastiera, tiramis, semifreddo, panna, sfogliatell, " +
                "cioccolat, frittell, strudel");
        Tag latte = new Tag("latte", "panna, latte, mozzarella, brie, gorgonzola, grana, ricott, formaggi, burro, " +
                "parmigiano, pecorino, feta, besciamella, fontina");
        Tag asporto = new Tag("asporto", "take away, away, asporto");
        Tag insalate = new Tag("insalate", "insalat");
        Tag primi = new Tag("primi", "prim, canederli, lasagne, cannelloni, gnocchi, pasta, risotto, penne, maccheroni, " +
                "tagliolini, tagliatelle, pizzoccheri, strangolapreti, fettuccine, farfalle, orecchiette, tortelloni, agnoli, agnolini, " +
                "agnolotti, paella, tortelli, tortellini, spaghetti");
        Tag frutta = new Tag("frutta", "albicocc, amarene, ananas, andguria, arance, arancia, avocado, babaco, goji, banana, cachi, carambola, " +
                "carrube, castagn, cedro, ciliegi, clementine, cocco, cocomero, curuba, fichi, fico, fragola, fragole, fragoline" +
                "fragolina, frutta, kiwi, lampone, lamponi, lime, limone, litchi, mandarini, mandarino, mango, mapo, marroni, mela, melagrana, " +
                "mele, melograno, melone, mirtilli, mirtillo, mora, nespole, more, nespole, papaia, pera, pere, pesca, pesche, platano, pompelmo, " +
                "prugna, prugne, ribes, susina, tamarindo, uva, visciole, yozu, olive");
        Tag fruttaSecca = new Tag("frutta secca", "anacardi, arachidi, datteri, mandorle, pinoli, uvetta, mandorla, mandorle, nocciola, nocciole, " +
                "noci, pistacchi, cocco");
        Tag funghi = new Tag("funghi", "champignon, chiodini, funghetti, funghi, porcini, tartufo");
        Tag salse = new Tag("salse", "salse, salsa, maionese, ketchup");
        Tag panini = new Tag("panini", "toast, panin");
        Tag domicilio = new Tag("domicilio", "domicilio");
        Tag uovo = new Tag("uovo", "uovo");
        Tag affumicato = new Tag("affumicato", "affumicat");
        Tag crescioni = new Tag("crescioni", "crescion");
        Tag focacce = new Tag("focacce", "focacc");
        Tag panzerotti = new Tag("panzerotti", "panzerott");
        Tag piade = new Tag("piade", "piade, piada, piadine, piadina");
        Tag tigelle = new Tag("tigelle", "tigell");
        Tag sushi = new Tag("sushi", "sushi, nigiri, uramaki, hosomaki");
        Tag riso = new Tag("riso", "riso");
        Tag vegano = new Tag("vegano", "vegan");
        Tag analcolico = new Tag("analcolico", "acqua, seltz, soda, succo");
        Tag alcolico = new Tag("alcolico", "acquavite, alcol, birra, bitter, brandy, cherry, cognac, gin, grappa, " +
                "liquore, marsala, mosto, prosecco, rhum, rum, sambuca, scotch, sherry, tequila, vermouth, vin, vino, vodka, whisky");
        Tag crostacei = new Tag("crostacei", "mazzancolle, moleche, aragosta, astice, canocchie, gamberetti, gambero, " +
                "scampi, crostacei, gamberoni, granchi, granchio, granciporro, scampi, scampo");
        Tag interiora = new Tag("interiora", "animelle, cervello, coregone, fegatini, fegato, midollo, milza, polmone, " +
                "regaglie, rene, rognone, trippa");
        Tag latticini = new Tag("latticini", "caglio, crema, kefir, latticello, latte, panna, yogurt");
        Tag legume = new Tag("legume", "borlotti, ceci, cicerchie, fagioli, ceci, piselli, soia, fave, lenticchie, lupini, soia");
        Tag mollusco = new Tag("mollusco", "lumaca, calamaretti, calamari, calamaro, canestrelli, cannolicchi, capasanta, capesante, " +
                "cappelunghe, cozza, cozze, fasolari, mitilo, molluschi, moscardini, moscardino, murice, seppia, ostrica, ostriche, peverasse piovra, " +
                "polipetti, polipi, polipo, polpo, ricci, seppia, seppie, seppioline, telline, totani, totano, vongola, vongole, zottoli");
        Tag pasta = new Tag("pasta", "anelli, anellini, bavette, bigoli, bucatini, cannelloni, capellini, capelli, cappelletti, caputini, " +
                "caserecce, cavatelli, cellentani, chifferi, chiocciole, chioccioline, conchiglie, conchigliette, conchiglioni, carallini, " +
                "ditali, ditalini, ditaloni, eliche, farfalle, farfalline, farfalloni, fazzoletti, fettucce, fettuccine, fidelini, fili, filini, fregola, " +
                "fusilli, garganelli, gemelli, gemmine, gigli, gnocchetti, gnocchi, gobbetti, gramigna, lagane, lancette, lasagne, lasagnette, linguine, lolli, " +
                "losanghe, maccaroncelli, maccarruni, maccherencelli, maccheroncini, maccheroni, mafaldine, maltagliati, mezzani, maniche, mezzemaniche, " +
                "nastrini, nidi, noodles, nuvole, orecchiette, paccheri, pappardelle, pasta, pastina, penne, pennette, pennoni, perciatelli, pici, pipe, " +
                "pizzoccheri, quadrucci, ravioli, raviolini, reginette, rigatoni, rotelle, ruote, sagnette, sedani, sedanini, spaghetti, spaghettini, spaghettoni, " +
                "stelline, strangolapreti, strangozzi, strozzapreti, tagliatelle, taglierini, tagliatelline, tagliolini, tempestina, tonnarelli, torciglioni, tortellini, " +
                "tortiglioni, trenette, torciglioni, tripoline, troccoli, trofie, trofiette, tubetti, tubettoni, vermicelli, vermicelloni, zite, ziti");

        addTag(pizza);
        addTag(vegetariano);
        addTag(formaggi);
        addTag(verdura);
        addTag(carne);
        addTag(selvaggina);
        addTag(salumi);
        addTag(pesce);
        addTag(spezie);
        addTag(piccante);
        addTag(dolci);
        addTag(latte);
        addTag(asporto);
        addTag(insalate);
        addTag(primi);
        addTag(frutta);
        addTag(salse);
        addTag(panini);
        addTag(domicilio);
        addTag(uovo);
        addTag(affumicato);
        addTag(crescioni);
        addTag(focacce);
        addTag(panzerotti);
        addTag(piade);
        addTag(tigelle);
        addTag(sushi);
        addTag(riso);
        addTag(vegano);
        addTag(analcolico);
        addTag(alcolico);
        addTag(crostacei);
        addTag(fruttaSecca);
        addTag(funghi);
        addTag(interiora);
        addTag(latticini);
        addTag(legume);
        addTag(mollusco);
        addTag(pasta);
    }

    public void check(String word){
        //controlliamo se una parola corrisponde a qualche tag
        for (int i=0; i<list.size(); i++){
            list.get(i).check(word);
            //if(list.get(i).found==true) {
            //    System.out.println(word);
            //}
        }
    }

    public void printTags(){
        for (int i=0; i<list.size(); i++){
            if(list.get(i).found==true){
                System.out.println(list.get(i).name);
            }
        }
    }

    public void printAll(){
        for (int i=0; i<list.size(); i++){
            list.get(i).printKeys();
            System.out.println(list.get(i).found);
        }
    }
}
