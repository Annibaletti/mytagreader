package mytagfinder;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import java.io.File;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Image {
    String text;
    TagDefiner tagDefiner = new TagDefiner();

    public Image(String filePath) throws TesseractException {
        Tesseract tesseract = new Tesseract ();
        tesseract.setDatapath("/usr/local/share/tessdata"); //dov'è la cartella tesseract
        text = tesseract.doOCR(new File(filePath));
    }

    public void lookForTag (){
        for (String retval: text.split("[\\r?\\n\\s+,-]")) {
            //splitta la stringa sugli a capo e i prezzi
            if (retval.length()>1) { //se abbiamo una parola
                tagDefiner.check(retval);
            }
        }
    }

    public void printTags (){
        tagDefiner.printTags();
    }

    public void printAllTags (){
        tagDefiner.printAll();
    }

}
